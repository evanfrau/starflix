import { writable } from "svelte/store";

import { browser } from "$app/env";

const FilmsStore = writable([]);

if (browser){
    FilmsStore.set(JSON.parse(localStorage.getItem("filmsStore")) || []);
    FilmsStore.subscribe(value => {
        localStorage.setItem("filmsStore", JSON.stringify(value));
    });
}


export default FilmsStore;