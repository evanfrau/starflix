import { writable } from "svelte/store";
import { browser } from "$app/env";

const UserStore = writable({});

if (browser){
    UserStore.set(JSON.parse(localStorage.getItem("userStore")) || {name: "padawan", password: "force"});
    UserStore.subscribe(value => {
        localStorage.setItem("userStore", JSON.stringify(value));
    });
}

export default UserStore;

