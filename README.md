# Starflix project

This is a project created for the Automation Battle Guild in order to test using Cypress. 

## Features

- Login to access the platform
- Change Name and Password
- See the list of the Star Wars films
- See the details of each films (summary, casting)
- See the details of each characters
- Search for a film or a character
- Add a film to the watchlist list
- Remove a film from the watchlist list

----

## Install the project locally

_Require Node.js to install to the dependencies and run the project._

```bash
git clone ...

# Install the dependencies
npm install
```

## Run it !

Once you've created a project and installed dependencies with `npm install` (or `pnpm install` or `yarn`), start a development server:

```bash
npm run dev

# or start the server and open the app in a new browser tab
npm run dev -- --open
```

## Building

To create a production version of your app:

```bash
npm run build
```

You can preview the production build with `npm run preview`.

> To deploy your app, you may need to install an [adapter](https://kit.svelte.dev/docs/adapters) for your target environment.
