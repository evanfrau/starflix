const config = {
	content: ['./src/**/*.{html,js,svelte,ts}'],

	theme: {
		extend: {},
		colors: {
			'swcolor': '#FFE819'
		}
	},

	 // add daisyUI plugin
	 plugins: [require("daisyui")],

	 // daisyUI config (optional)
	 daisyui: {
	   styled: true,
	   daisyui: {
		themes: ["light", "dark", "cupcake", "bumblebee", "emerald", "corporate", "synthwave", "retro", "cyberpunk", "valentine", "halloween", "garden", "forest", "aqua", "lofi", "pastel", "fantasy", "wireframe", "black", "luxury", "dracula", "cmyk", "autumn", "business", "acid", "lemonade", "night", "coffee", "winter"],
	  },
	   base: true,
	   utils: true,
	   logs: true,
	   rtl: false,
	   prefix: "",
	   darkTheme: "dark",
	 },
};

module.exports = config;
